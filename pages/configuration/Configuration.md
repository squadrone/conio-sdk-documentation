# Inizializzazione dell'SDK

## L'oggetto Conio

Per usare l'SDK occorre inizializzare l'oggetto `Conio` con una `ConioConfiguration`. La configurazione determinerà l'ambiente con il quale l'SDK interagirà. 

E' supportata la configurazione di `test` che si interfaccerà con l'ambiente di staging per lavorare con la rete Bitcoin testnet.

E' possibile utilizzare l'ambiente di produzione `prod` per lavorare con rete Bitcoin mainnet.

Oltre alle configurazioni di `test` e `prod` è possibile inizializzare l'SDK con un ambiente personalizzato, specificando l'url del backend e la rete Bitcoin da utilizzare. Di seguito le specifiche per inizializzare un oggetto di tipo `Conio`.

### Parametri

- **configuration**: la configurazione per inizializzare l'SDK di tipo [ConioConfiguration](#conio-configuration)
- (Android) **context**: il context che sfrutterà il salvataggio sulle Shared Preferences

### Conio Configuration

- **identifier**: il nome della configurazione
- **bitcoinNetwork**: la rete Bitcoin. Può essere `.testnet` o `.mainnet`
- **networkEnvironment**: l'ambiente utilizzato per connettersi al backend di tipo [NetworkEnvironment](#network-environment)

### Network Environment

- **name**: il nome dell'ambiente
- **host**: l'host da utilizzare come backend

### Codice

#### Android
```java
import com.conio.sdk.Conio;
import com.conio.sdk.models.shared.BitcoinNetwork;
import com.conio.sdk.models.shared.ConioConfiguration;
import com.conio.sdk.providers.networking.NetworkEnvironment;

// Test configuration
Conio conio = new Conio(ConioConfiguration.test, getApplicationContext());

// Production configuration
Conio conio = new Conio(ConioConfiguration.prod, getApplicationContext());
```

#### iOS
```swift
import ConioSDK

// Test configuration
let conio = Conio(configuration: ConioConfiguration.test)

// Production configuration
let conio = Conio(configuration: ConioConfiguration.prod)
```
