# Operazioni sul portafoglio

## Lettura dell'indirizzo Bitcoin

Permette di recuperare l'indirizzo corrente del portafoglio su cui sarà possibile ricevere delle transazioni.

### Risposta

Una `stringa` contenente l'indirizzo Bitcoin attuale.

### Codice

#### Android

```java
conio.walletService.currentBitcoinAddress(result->{
    result.analysis(address-> {
        // String
    }, error-> {
        // Exception
    });
});
```

#### iOS

```swift
conio.walletService.currentBitcoinAddress { result in   
    result.analysis(ifSuccess: { address in
        // String
    }, ifFailure: { error in
        // ServiceError
    })
}
```


## Lista movimenti bitcoin

Ciascuna operazione di invio, ricezione, acquisto e vendita di Bitcoin è rappresentata da un'`Activity`. La lista delle attività svolte dall'utente può essere recuperata tramite l'apposito metodo.

### Parametri

Per ottenere la lista dei movimenti del wallet passeremo all'SDK una struct di tipo `ActivitiesParams` contenente:

- **index**: l'indice di partenza con cui recuperare le `6 attività` successive. Con indice 0 si recupereranno le attività fino all'indice 5. La seconda chiamata, dunque, avrà come indice 6 e permetterà di recuperare le attività sino all'indice 11. 
**Nota**: per recuperare tutte le attività è opportuno ripetere questa chiamata finché verranno restituite meno di 6 attività: questo è l'indicatore che si è arrivati alla fine della lista

- **types**: un array di enumerati che ci permette di specificare le tipologie di activities da recuperare. È disponibile una variabile statica denominata `all` che compone un array contenente tutti i tipi.

- **currency**: possibilità di recuperare le activity con i valori riferiti alla transazione specificati in Euro o Dollari

### Risposta 
Una struct di tipo `WalletActivities` contenente:
-  **activities**: ovvero un array di `Activity` dell'utente.
Ogni activity ha un suo `ID` univoco, un `Type` (send, buy, sell, receive), un ammontare in `Satoshi`, un `confirmationStatus` che permette facilmente di discriminare fra transazioni confermate o no.
### Codice

#### Android

```java
// To retrieve all the activities
ActivityType[] types = ActivityType.all()
ActivitiesParams params = new ActivitiesParams(0, types, Currency.EUR);

// To retrieve purchases and sells
ActivityType[] types = new ActivityType[] { 
    ActivityType.BUY, 
    ActivityType.SELL 
};

ActivitiesParams params = new ActivitiesParams(0, types, Currency.EUR);

conio.walletService.walletActivities(params, result->{
    result.analysis(activities-> {
        // WalletActivities
    }, error-> {
        // Exception
    });
});
```

#### iOS

```swift

// To retrieve all the activities
let params = ActivitiesParams(
    index: 0, 
    types: ActivityType.all
    currency: .eur
)

// To retrieve only purchases and sells
let params = ActivitiesParams(
    index: 0, 
    types: [.buy, .sell]
    currency: .eur
)

conio.walletService.walletActivities(params: activitiesParams) { result in
    result.analysis(ifSuccess: { activities in
        // WalletActivities
    }, ifFailure: { error in
        // ServiceError
    })
}
```

## Dettaglio di un'attività

Recuperata la lista delle attività è possibile ottenere ulteriori informazioni richiedendone il dettaglio. È il caso delle attività di acquisto e vendita, delle quali si può accedere ad informazioni legate al metodo di pagamento utilizzato, all'iban di destinazione e così via. Per le transazioni in uscita ed in entrata, allo stesso modo si possono leggere i valori legati alle conferme sulla blockchain ad esempio.

### Parametri

Un oggetto di tipo `ActivityDetailsParams` contenente:

- **activityId**: l'id dell'attività per la quale si vuole leggere il dettaglio
- **currency**: la valuta che si vuole usare per ottenere le informazioni legate alla transazione

### Risposta

Un `ActivityDetails` contenente:

- **id**: l'id dell'attività
- **type**: la tipologia dell'attività
- **timestamp**: il timestamp che rappresenta il momento in cui è stata creata l'attività
- **@Opzionale transaction**: la transazione annessa. Contiene informazioni quali l'hash della transazione, le conferme in blockchain, lo stato della conferma, gli indirizzi dei destinatari, il controvalore in valuta corrente, le fee pagate per transare, la fee per byte e così via. Popolato nel caso in cui sia stata effettuata una transazione.
- **@Opzionale bid**: la bid annessa. Contiene le informazioni sulla richiesta di acquisto. Popolato in caso di attività di acquisto.
- **@Opzionale payment**: le informazioni collegate al pagamento, incluso l'id del metodo di pagamento utilizzato, il valore del pagamento e lo stato. Popolato in caso di attività di acquisto.
- **@Opzionale ask**: l'ask annessa. Contiene le informazioni sulla richeista di vendita. Popolato in caso di attività di vendita.
- **@Opzionale sell**: le informazioni collegate alla vendita, incluso l'id del metodo di pagamento, l'iban del destinatario, il cro dell'operazione e lo stato. Popolato in caso di attività di vendita.

### Codice

#### Android

```java
ActivityDetailsParams params = new ActivityDetailsParams("id", Currency.EUR);    

conio.walletService.activityDetails(params, result->{
    result.analysis(details-> {
        // ActivityDetails
    }, error-> {
        // Exception
    });
});
```

#### iOS

```swift
let params = ActivityDetailsParams(activityId: "activityId", currency: .eur)

 conio.walletService.activityDetails(params: params) { result in
    result.analysis(ifSuccess: { details in
        // ActivityDetails                 
    }, ifFailure: { error in
        // ServiceError               
    })
}
```

## Dettagli del portafoglio
Permette di recuperare l'ammontare presente sul wallet.

### Risposta 
Un `WalletDetails` contenente il valore di bitcoin presente nel wallet espresso in satoshi (1 bitcoin = 100.000.000). Il valore si divide in:

- **confirmedBalance**: valore con almeno 3 conferme sulla blockchain Bitcoin e quindi disponibile per l'utente.
- **unconfirmedBalance**: valore con meno di 3 conferme e quindi non ancora disponibile.

### Codice

#### Android

```java
conio.walletService.walletdetails(result->{
    result.analysis(details-> {
        // WalletDetails
    }, error-> {
        // Exception
    });
});
```

#### iOS

```swift
conio.walletService.walletDetails { result in
    result.analysis(ifSuccess: { details in
        // WalletDetails
    }, ifFailure: { error in
        // ServiceError
    })
}
```

## Ottenimento delle fee raccomandate per invio di Bitcoin
Per poter spostare bitcoin dal wallet verso un altro indirizzo bitcoin occorre stabilire la quantità di commissione da pagare ai miner.
Per farlo Conio offre un algoritmo di stima delle commissioni in base al tempo che si è disposti ad attendere per vedere la propria transazione inserita in blockchain.

### Parametri

Si utilizza una struct di tipo `WithdrawalFeesParams` inizializzabile con:

- **destAddress**: l'indirizzo bitcoin di destinazione
- **amount**: il valore della transazione in Satoshi, inserire `0` nel caso si voglia inviare tutto l'ammontare disponibile.
- **speed**: la velocità della transazione (1 più veloce, 5 più lenta), se non specificato si otterrà nella risposta un array con tutte le speed disponibili

### Ottenimento del massimo inviabile
Per prima cosa occorrerà richiedere all'SDK la quantità di Bitcoin che il wallet può inviare.
Questo servirà per permetterci di segnalare all'utente se sta inserendo un importo maggiore di quanto sia possibile.
Per fare questo basterà fare una richiesta `WithdrawalFees` passando come parametro solamente l'indirizzo di destinazione.

### Ottenimento di tutte le velocità di invio
- Se si vuole dare la possibilità all'utente di decidere la velocità si dovranno passare i seguenti parametri: **destAddress** e **amount**. 
Si otterrà in risposta dal server un array contenente le velocità dispobili e il relativo costo in **feePerByte** cosi che sia possibile mostrare all'utente le varie opzioni.

### Risposta

Una `AvailableWithdrawalFees` contenente:

- **absoluteFees**: ammontare complessivo delle fee in satoshi
- **absoluteFeesFiat**: costo delle fee in valuta euro
- **feePerByte**: dimensione in byte della transazione/absoluteFee = feePerByte
- **transactionSpeed**: la velocità della transazione
- **availableFees**: array contenente tutte le altre velocità disponibili
### Errori

* NOT_ENOUGH_BTC_AMOUNT Bitcoin disponibili non sufficienti
* DUST_TRANSACTION Ammontare Bitcoin troppo piccolo
* NO_SUCH_WITHDRAWAL_FEES_INFO: Non sono presenti fees ammissibili
### Codice
#### iOS

```swift
// ottenimento del massimo inviabile dal wallet a velocita piu lenta (5)
let params = WithdrawalFeesParams(destAddress: "mkHS9ne12qx9pS9VojpwU5xtRd4T7X7ZUt")

// ottenimento di tutte le velocità disponibili per un certo ammontare (ad esempio 10mila satoshi)
let params = WithdrawalFeesParams(destAddress: "mkHS9ne12qx9pS9VojpwU5xtRd4T7X7ZUt", amount: 10_000)

// ottenimento esclusivamente della fee da pagare per inviare TUTTO nel minor tempo possibile (speed 1)
let params = WithdrawalFeesParams(destAddress: "mkHS9ne12qx9pS9VojpwU5xtRd4T7X7ZUt", amount: 0, speed: .transactionSpeedType1)

conio.walletService.withdrawalFees { result in
    result.analysis(ifSuccess: { availableFees in
        // availableFees
    }, ifFailure: { error in
        // ServiceError
    })
}
```
#### Android

```java
// ottenimento del massimo inviabile dal wallet a velocita' piu lenta (5)
let params = WithdrawalFeesParams(destAddress: "mkHS9ne12qx9pS9VojpwU5xtRd4T7X7ZUt")

// ottenimento di tutte le velocità disponibili per un certo ammontare (ad esempio 10mila satoshi)
let params = WithdrawalFeesParams(destAddress: "mkHS9ne12qx9pS9VojpwU5xtRd4T7X7ZUt", amount: 10_000)

// ottenimento esclusivamente della fee da pagare per inviare TUTTO nel minor tempo possibile (speed 1)
WithdrawalFeesParams params = new WithdrawalFeesParams("mkHS9ne12qx9pS9VojpwU5xtRd4T7X7ZUt", 0L, TransactionSpeedType.SPEED_ONE);

conio.walletService.withdrawalFees(params, response -> {
    response.analysis(availableFees -> {
        // availableFees
    }, error -> {
        // ServiceError
    });
});
```
## Invio Bitcoin

L'SDK di Conio permette di movimentare fondi verso altri portafogli. Prima di effettuare un'operazione di invio, assicurarsi che i fondi a disposizione siano sufficienti effettuando la chiamata per [ottenere il massimo inviabile](#Ottenimento-del-massimo-inviabile).
Inoltre sarà necessario superare una verifica supplementare inserendo, nei parametri della operazione, il codice che il cliente riceverà tramite email per completare con successo l'invio.

### Come gestire la verifica?

Dopo la prima invocazione del metodo, verrà restituito un errore che indicherà la necessità di richiedere il codice di conferma all'utente. Una mail partirà all'utente contestualmente a questa operazione. Il codice avrà validità di **5 minuti**. 

Questo codice va utilizzato, successivamente, nei parametri dello stesso metodo (sono disponibili due costruttori, uno con codice ed uno senza). Il risultato è che in totale ci saranno due chiamate al metodo di invio. Una usando il costruttore che specifica soltanto i dati della transazione in uscita. Usa usando il costruttore con il `MFA Token` e `Codice MFA`.

### Recupero del MFA Token

#### iOS

```swift
let params = TransactionParams(
    address: "mkHS9ne12qx9pS9VojpwU5xtRd4T7X7ZUt", 
    amount: UInt64(1000), 
    feePerByte: UInt64(10)
)
conio.walletService.sendTransaction(params) { result in
    result.analysis(ifSuccess: { sentTransaction in
        // ...
    }, ifFailure: { error in
        // Get the mfa token
        if case let .mfaRequired(token: token) = error {
            let mfaToken = token
        }
    })
}
```

#### Android


```java
// Chiamata senza Codice MFA ed MFA Token
TransactionParams params = new TransactionParams(
    "mkHS9ne12qx9pS9VojpwU5xtRd4T7X7ZUt",
    1000L,
    10L
);

conio.walletService.sendTransaction(params, result -> {
    result.analysis(sentTransaction -> {
        // ...
    }, error -> {
        // Get the mfa token
        if (error instanceof MfaRequiredException) {
            MfaRequiredException mfaRequiredException = (MfaRequiredException) error;
            String mfaToken = mfaRequiredException.getMfaToken();
        }
    }
    });
});
```

### Utilizzo del deep link

Il deeplink da registrare avrà una struttura di questo tipo

```
conio-internal://request_btc_withdrawal?code=<MFACode>
```

Quando si lavora con i deep link, bisogna gestire la casistica dell'app che reinizializza il suo stato iniziale, facendo eventualmente perdere le informazioni necessarie per mostrare all'utente la schermata di riepilogo.

Per ovviare a questo problema, sarà necessario serializzare le informazioni che si vogliono recuperare (bitcoin da inviare, indirizzo e fee per byte da usare), salvandole in locale. Non trattandosi di informazioni sensibili qualsiasi metodo di storage è consono.

### Parametri

Per inviare bitcoin, si utilizza una struct di tipo `TransactionParams` contenente:

- **address**: indirizzo del destinatario
- **amount**: il valore della transazione in Satoshi
- **fee**: la fee, espressa in Satoshi per byte
- **mfaCode**: per ottenere questo valore occorre procedere con un Invio senza questo parametro. L'utente riceverà una mail contenente il codice mfaCode con il quale procedere nuovamente all'Invio.

### Risposta

Una `SentTransaction` contenente:

- **transactionId**: l'id della transazione
- **fee**: le fee della transazione

### Errori
- **[iOS] ServiceError.mfaRequired**: Il codice mfa non è corretto o è scaduto oppure non è stato inserito. In ogni caso l'utente riceverà una mail con un nuovo codice.
-  **[Android] ConioError.MFA_REQUIRED**: Il codice mfa non è corretto o è scaduto oppure non è stato inserito. In ogni caso l'utente riceverà una mail con un nuovo codice.
* NOT_ENOUGH_BTC_AMOUNT Bitcoin disponibili non sufficienti
* DUST_TRANSACTION Ammontare Bitcoin troppo piccolo
* INVALID_MESSAGE_SIGNATURE La richiesta è stata manomessa 

### Codice da usare dopo aver ottenuto i token

#### Android

```java
// Chiamata con Codice MFA e MFA Token
TransactionParams params = new TransactionParams(
    "mkHS9ne12qx9pS9VojpwU5xtRd4T7X7ZUt", 
    1000L,
    10L,
    "OciqYgdjxJV413iHkFqgUYGk",
    "806157"
);

conio.walletService.sendTransaction(params, result -> {
    result.analysis(sentTransaction -> {
        // SentTransaction
    }, error -> {
        // Exception
    });
});
```

#### iOS

```swift
let params = TransactionParams(
    address: "mkHS9ne12qx9pS9VojpwU5xtRd4T7X7ZUt", 
    amount: UInt64(1000), 
    feePerByte: UInt64(10),
    mfaToken: "OciqYgdjxJV413iHkFqgUYGk",
    mfaCode: "806157"
)

conio.walletService.sendTransaction(params) { result in
    result.analysis(ifSuccess: { sentTransaction in
        // SentTransaction
    }, ifFailure: { error in
        // ServiceError
    })
}
```

## Visualizzazione delle 12 parole di backup (mnemonica)
Permette di recuperare dalla memoria del dispositivo le 12 parole di backup del portafoglio Bitcoin.

### Risposta

Un oggetto di tipo `MnemonicWords` contenente un array di 12 stringhe.

### Errori
- **missingMnemonic**: non è stato possibile trovare in memoria i dati 
### Codice

#### Android

```java
conio.walletService.readMnemonic(result->{
    result.analysis(mnemonicWords-> {
        // MnemonicWords
    }, error-> {
        // Exception
    });
});
```

#### iOS

```swift
conio.walletService.readMnemonic { result in 
    result.analysis(ifSuccess: { mnemonicWords in
        // MnemonicWords
    }, ifFailure: { error in
        // ServiceError
    })
}
```
