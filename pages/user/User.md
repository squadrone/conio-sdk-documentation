# Operazioni sull'utente

## Accettazione dei termini di servizio

Questa operazione consente di recuperare `Acceptances` da mostrare all'utente. Esse verranno inviate in fase di signup, assieme alla decisione dell'utente di accettarle o meno. Oltre alle `Acceptances`, verranno inoltre restituiti gli url per mostrare le pagine dei Termini di Servizio e Privacy Policies di Conio.  

### Parametri

Un oggetto di tipo `LegalAcceptancesParams` contenente la lingua di riferimento per ottenere le acceptances e gli url delle pagine web da mostrare all'utente.

### Risposta

Un oggetto di tipo `LegalAcceptances` contenente le `Acceptances`, l'url dei `Termini di Servizio` e quello delle `Privacy Policies`.

### Localizzazione delle Acceptances

Un oggetto di tipo `Acceptance` contiene, tra le altre cose, due chiavi di localizzazione. Una per il titolo dell'acceptance ed una per il suo contenuto.

### Codice

#### Android

```java
LegalAcceptancesParams params = 
    new LegalAcceptancesParams(Language.ITALIAN);
conio.userService.getLegalAcceptances(params, result -> {
    result.analysis(acceptances -> {
        // LegalAcceptances
    }, error -> {
        // Exception
    });
});
```

#### iOS

```swift
let params = LegalAcceptancesParams(language: .italian)

conio.userService.getLegalAcceptances(params: params) { result in
    result.analysis(ifSuccess: { legalAcceptances in
        // LegalAcceptances
    }, ifFailure: { error in
        // ServiceError
    })
}
```

## Signup

Per poter operare con il portafoglio Conio occorre essere autenticati. Se è la prima volta che l'utente usa il servizio ci si può autenticare con il metodo Signup, altrimenti con il metodo login.

### Parametri

Una struttura di tipo `Account` contenente le seguenti strutture:

- ***login***: di tipo `Login` per iOS e `UserLogin` per Android con username e password dell'utente
- ***acceptances***: di tipo `Acceptances` con l'esito della conferma ai termini di servizio
- ***cryptoRequest***: di tipo `CryptoRequest` la richiesta firmata seguendo la [creazione della Crypto Request](#Creazione-della-Crypto-Request)

### Creazione della Crypto Request

Per generare una `Crypto Request`, è necessario firmare la stringa `dataString`, generata leggendo l'esempio sottostante, tramite sha256 utilizzando la chiave privata. Gli esempi sono generati con dello pseudocodice a fini dimostrativi, l'implementazione dell'algoritmo di firma del dato da inglobare nella `CryptoRequest` è prerogativa del client.

#### Esempio in Java

```java
    String proofId = UUID.randomUUID().toString();
    long proofExpiration = 
        new Date()
        .tenMinutesFromNow()
        .millis();
        
    String userLevel = "A smart level"; // Es. "Advanced" to get adavanced limits
    String userId = login.username;
    String iban = "IBAN"; // It should be a real iban
    String email = "user@email.com";
    String firstName = "Mario";
    String lastName = "Rossi";

    String[] data = {
        proofId, 
        "SIGNUP", 
        userId, 
        userLevel, 
        String.valueOf(proofExpiration), 
        iban,
        email,
        firstName,
        lastName
    };

    String dataString = join("|", data);

    PrivateKey privateKey = new PrivateKey("key.pem");
    RsaSigner rsa = new RsaSigner(privateKey);

    String signature = 
        rsa
        .sign("sha256", dataString)
        .toLowercase();

    byte[] cryptoProof = fromHexToBytes(signature);
```

#### Esempio in Swift

```swift
    let proofID = UUID().uuidString
    let proofExpiration: UInt64 = UInt64(Date())
    let userLevel = "A smart level" // Es. "Advanced" to get adavanced limits
    let userID = login.username
    let iban = "IBAN" // It should be a real iban
    let email = "user@email.com"
    let firstName = "Mario"
    let lastName = "Rossi"

    let data = [
        proofID,
        "SIGNUP",
        userID,
        userLevel,
        String(proofExpiration),
        iban,
        firstName,
        lastName
    ]

    let dataString = data.joined(separator: "|")

    let cryptoProof = Crypto.sign(
        privateKey: privateKey, 
        digestType: .sha256
    )

    let cryptoRequest = CryptoRequest(
        proofID: proofID,
        cryptoProof: cryptoProof.data,
        proofExpiration: proofExpiration,
        userID: userID,
        userLevel: userLevel,
        iban: iban,
        email: email,
        firstName: firstName,
        lastName: lastName
    )
```

### Risposta 

Un oggetto di tipo `Acceptances` con le condizioni accettate dall'utente in fase di Signup.

### Errori
* `INVALID_IBAN` IBAN non valido
* `CRYPTO_PROOF_EXPIRED` La crypto proof è scaduta
* `INVALID_CRYPTO_PROOF` La crypto proof non è correttamente firmata
* `CARDS_SERVICE_COULD_NOT_CREATE_PAYER` Errore interno del sottosistema di pagamento
* `DUPLICATE_EMAIL_ADDRESS` Indirizzo email duplicato
* `WALLET_ALREADY_OWNED_BY_ANOTHER_USER` Il wallet è già utilizzato da un altro utente
* `CLIENT_SUPPORT_ACCEPTANCE_NOT_ACCEPTED` Acceptance obbligatoria
* `APP_IMPROVEMENT_ACCEPTANCE_NOT_ACCEPTED` Acceptance obbligatoria


### Codice

#### Android

```java
UserLogin login = new UserLogin("lemonade", "secretword");

// Build the acceptances list with the user choices result
Acceptance appImprovement 
    = new Acceptance(AcceptanceType.APP_IMPROVEMENT, true);
Acceptance clientSupport 
    = new Acceptance(AcceptanceType.CLIENT_SUPPORT, true);

ArrayList<Acceptance> acceptanceList = new ArrayList<>();
acceptanceList.add(appImprovement);
acceptanceList.add(clientSupport);

Acceptances acceptances = new Acceptances(acceptanceList);

// Your crypto request implementation
CryptoRequest cryptoRequest = buildCryptoRequest();

Account account = new Account(login, acceptances, cryptoRequest);
conio.userService.signup(account, result -> {
    result.analysis(acceptances -> {
        // Acceptances
    }, error -> {
        // Exception
    });
});
```

#### iOS

```swift

let login = Login(username: "lemonade", password: "secretword")

// Your crypto request implementation
let cryptoRequest = buildCryptoRequest()

// Build the acceptances list with the user choices result
let appImprovement = 
    Acceptance(type: .appImprovement, isAccepted: true)
let clientSupport = 
    Acceptance(type: .clientSupport, isAccepted: true)

let acceptancesList = [appImprovement, clientSupport]
let acceptances = Acceptances(acceptances: acceptancesList)

let account = Account(
    login: login,
    acceptances: acceptances,
    cryptoRequest: cryptoRequest
)

conio.userService.signup(with: account) { result in
    result.analysis(ifSuccess: { acceptances in
        // Acceptances
    }, ifFailure: { error in
        // ServiceError
    })
}
```

## Login

L'operazione di login permette di autenticarsi a Conio. È **raccomandabile** eseguire questa operazione ad ogni avvio dell'applicazione, similmente a come avviene per altri servizi terzi.

### Parametri

Un oggetto di tipo `Login` per iOS o `UserLogin` per Android contenente

- **username**: lo username dell'utente
- **password**: la password dell'utente

### Risposta

Un oggetto di tipo `Acceptances` con le condizioni accettate dall'utente in fase di Signup.

### Codice

#### Android

```java
UserLogin login = new UserLogin("lemonade", "secretword");
conio.userService.login(login, result -> {
    result.analysis(acceptances -> {
        // Acceptances
    }, error -> {
        // Exception
    });
});
```

#### iOS

```swift
let login = Login(username: "lemonade",password: "secretword")
conio.userService.login(with: login) { result in
    result.analysis(ifSuccess: { acceptances in
        // Acceptances
    }, ifFailure: { error in
        // ServiceError
    })
}
```

## Logout

Consente di disconnettere l'utenza Conio.

### Risposta

Un `booleano` con l'esito dell'operazione.

### Codice

#### Android

```java
conio.userService.logout(result -> {
    result.analysis(success -> {
        // Boolean
    }, error -> {
        // Exception
    });
});
```

#### iOS

```swift
conio.userService.logout { result in
    result.analysis(ifSuccess: { success in
        // Boolean
    }, ifFailure: { error in
        // ServiceError
    })
}
```