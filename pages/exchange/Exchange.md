# Operazioni sul mercato

## Prezzo attuale del Bitcoin

È possibile recuperare il prezzo attuale, di acquisto e di vendita, del Bitcoin specificando la valuta nel quale lo si vuole ottenere. Inoltre, l'SDK offre la possibilità di fare la conversione di un ammontare in Bitcoin nella valuta di riferimento.

### Parametri

Un oggetto di tipo `CurrentPriceParams` contenente:

- **currency**: la valuta in cui si vuole ottenere il prezzo
- **@Opzionale amount**: l'ammontare di valuta corrente che si vuole convertire in Bitcoin

### Risposta

Un `CurrentPrice` contenente:

- **buyPrice**: il prezzo di acquisto
- **sellPrice**: il prezzo di vendita
- **timestamp**: l'ora di campionamento del prezzo

### Codice

#### Android

```java
// Recupero del prezzo attuale
CurrentPriceParams params = new CurrentPriceParams(Currency.EUR);

// Conversione di 50.000.000 satoshi (0,5 BTC) in euro
CurrentPriceParams params = new CurrentPriceParams(Currency.EUR, 50_000_000L)

conio.exchangeService.currentPrice(params, result->{
  result.analysis(price-> {
    // CurrentPrice
  }, error-> {
     // Exception
  });
});
```

#### iOS
```swift
// Recupero del prezzo attuale
let params = CurrentPriceParams(currency: .eur)

// Conversione di 50.000.000 satoshi (0,5 BTC) in euro
let params = CurrentPriceParams(currency: .eur, satoshiAmount: 50_000_000)

conio.exchangeService.currentPrice(params: params) { result in
 result.analysis(ifSuccess: { prices in
      // CurrentPrice
    }, ifFailure: { error in
      // ServiceError
    })
});
```

## Prezzo storico del Bitcoin

È possibile recuperare il prezzo storico del Bitcoin selezionando l'inizio e la fine dell'intervallo di riferimento.

### Parametri

Un oggetto di tipo `HistoricalPriceParams` contenente:

- **currency**: la valuta in cui si vuole ottenere il prezzo
- **startTimestamp**: il timestamp dell'inizio dell'intervallo
- **endTimestamp**: il timestamp della fine dell'intervallo
- **@Opzionale interval**: l'intervallo che si vuole porre trai prezzi restituiti

### Risposta

Un `HistoricalPrices` contenente:

* Una lista di `CurrentPrice`, documentata nel paragrafo precedente
* Un oggetto `PriceAnalytics` contenente:
  * **deltaFiat**: la variazione in valuta, del prezzo del Bitcoin, dall'inizio del periodo di riferimento
  * **deltaPercentage**: la variazione in percentuale, del prezzo del Bitcoin, dall'inizio del periodo di riferimento
  * **trend**: un enumerato che rappresenta se il prezzo del Bitcoin, dall'inizio del periodo di riferimento, è cresciuto, è diminuito o è rimasto stagnante

### Codice

#### Android

```java
// Prezzo dal 16 aprile 2019 al 16 aprile 2018
// Intervallo standard: 1 giorno
HistoricalPriceParams params = new HistoricalPriceParams(
  Currency.EUR, 
  1523885446000L, 
  1563465540000L
);

// Prezzo dal 16 aprile 2019 al 16 aprile 2018
// Intervallo selezionato: 1 settimana
HistoricalPriceParams params = new HistoricalPriceParams(
  Currency.EUR, 
  1523885446000L, 
  1563465540000L,
  604800000
);

conio.exchangeService.historicalPrices(params, result->{
  result.analysis(prices-> {
    // HistoricalPrices
  }, error-> {
    // Exception
  });
});
```

#### iOS
```swift
// Prezzo dal 16 aprile 2019 al 16 aprile 2018
// Intervallo standard: 1 giorno
let params = HistoricalPriceParams(currency: .eur, startTimestamp: 1523885446000, endTimestamp: 1563465540000)

// Prezzo dal 16 aprile 2019 al 16 aprile 2018
// Intervallo selezionato: 1 settimana
let params = HistoricalPriceParams(currency: .eur, startTimestamp: 1523885446000, endTimestamp: 1563465540000, interval: 604800000)

conio.exchangeService.historicalPrices(params: params) { result in
 result.analysis(ifSuccess: { prices in
      // HistoricalPrices
    }, ifFailure: { error in
      // ServiceError
    })
});
```

## Recupero limiti di trading

Per recuperare i limiti associati ad un utente, assegnati in fase di signup utilizzando come User Level il valore `"advanced"`, esiste un metodo dedicato.

### Risposta

Un oggetto di tipo `AllTradingLimits (Android)` o `Limits (iOS)` con all'interno:

- Due oggetti di tipo `TradingLimits`, uno che rappresenta quelli per gli acquisti ed uno per le vendite. All'interno di un `TradingLimits` troviamo:

    - **currentLimit**: il limite attualmente a disposizione
    - **limits**: una lista di limiti, ciascuno conterrà la rispettiva tipologia (Giornaliero, Mensile, Attuale) ed il valore per ciascuno di essi.
    - **currentLimitsByType**: il valore residuo per ciascuno dei limiti contenuti nell'oggetto `limits` del punto precedente.

- **minimumBuyAmount**: l'importo minimo in fiat (EUR) per procedere con l'acquisto di Bitcoin
- **minimumSellAmount**: l'importo minimo in fiat (EUR) per procedere con la vendita di Bitcoin

### Codice

#### Android

```java
conio.exchangeService.tradingLimits(result -> {
  result.analysis(limits -> {
    // TradingLimits
  }, error -> {
    // Exception
  });
});
```

#### iOS
```swift
conio.exchangeService.tradingLimits { result in
 result.analysis(ifSuccess: { limits in
      // TradingLimits
    }, ifFailure: { error in
      // ServiceError
    })
});
```

## Acquisto di Bitcoin

Per poter acquistare dei Bitcoin è necessario effettuare due operazioni. La prima è quella di creazione di una `Bid`, ovvero di una richiesta di acquisto di una determinata somma di Bitcoin ad un certo prezzo. All'interno della `Bid` si troveranno le `WiretransferInfo` che dovranno essere usate dal client per effettuare il bonifico di acquisto. Infine si dovrà utilizzare la seconda operazione verso Conio per comunicare l'avvenuto pagamento tramite bonifico della `Bid` allegando anche una `CryptoProof`, generata client side, per testimoniare la legittimità dell'operazione.

## Creazione della Bid

Una `RequestBid` si crea specificando la **valuta** che si intende utilizzare per la transazione e l'importo, tassativamente o in satoshi, o in valuta corrente. Sarà quindi possibile richiedere una `RequestBid` in Euro per l'acquisto di 20€ di Bitcoin, o una `RequestBid` in euro per l'acquisto di 100.000.000 satoshi. 

Una volta inviata la richiesta, si otterrà una `Bid` contenente, tra le altre informazioni un `ID`. Con questo identificativo sarà possibile **aggiornare** la richiesta di Bid per rimandarne la scadenza e per ottenere le informazioni sul tasso di cambio più aggiornate. Questo scenario è utile nei casi in cui tra la richiesta della Bid e l'effettiva azione dell'utente passi del tempo che renderebbe il tasso di cambio obsoleto.

### Parametri

- **(opzionale) id**: l'id della bid, da valorizzare solo in caso di refresh della bid stessa
- **(one of) satoshi**: l'ammontare in satoshi per cui si vuole conoscere il corrispettivo in euro
- **(one of) fiatAmount**: l'ammontare in valuta corrente per cui si vuole conoscere il corrispettivo in satoshi
- **currency**: la valuta dell'operazione

Nell'SDK sono stati implementati dei costruttori che permettono di valorizzare solo un'informazione alla volta, tra **satoshi** o **fiatAmount**. In nessun caso devono essere presenti entrambi contemporaneamente.

### Risposta

Un oggetto di tipo `CreatedBid` che contiene:

- **id**: l'id utile al refresh o alla finalizzazione della bid
- **currency**: la valuta dell'operazione
- **satoshi**: l'ammontare in satoshi della richiesta d'acquisto
- **fiatAmount**: l'ammontare in valuta corrente della richiesta d'acquisto
- **serviceFees**: le commissioni di servizio per la transazione, espresse in nella **currency** di riferimento
- **expiration**: il timestamp di scadenza della richiesta di pagamento. Se la bid scade sarà necessario aggiornarla per proseguire
- **wiretransferInfo**: le informazioni necessarie per procedere al pagamento della Bid tramite bonifico.

### Codice

#### Android

```java

// Richiesta d'acquisto per 100€
CreateOrRefreshBidParams params = 
  new CreateOrRefreshBidParams(Currency.EUR, 100d);

// Richiesta d'acquisto per 1.000.000 satoshi
CreateOrRefreshBidParams params = 
  new CreateOrRefreshBidParams(Currency.EUR, 100000000l);

// Aggiornamento di una richiesta d'acquisto per 100€
CreateOrRefreshBidParams params = 
  new CreateOrRefreshBidParams(
    "bididentifier", 
    Currency.EUR, 
    100d
  );

conio.exchangeService.createOrRefreshBid(params, result -> {
  result.analysis(bid -> {
    // CreatedBid
  }, error -> {
    // Exception
  });
});
```

#### iOS
```swift
// Richiesta d'acquisto per 50€
let params = CreateOrRefreshBidParams(currency: .eur, fiatAmount: 50.0)

// Richiesta d'acquisto per 1.000.000 satoshi
 let params = CreateOrRefreshBidParams(currency: .eur, satoshi: 1000000)
 
// Aggiornamento di una richiesta d'acquisto per 100€
let params = CreateOrRefreshBidParams(bidID: "bididentifier", currency: .eur, fiatAmount: 100.0)

conio.exchangeService.createOrRefreshBid(params: params) { result in
 result.analysis(ifSuccess: { createdBid in
        // CreatedBid
    }, ifFailure: { error in
        // ServiceError
    })
});
```

## Utilizzo della Bid (pagamento)

Una volta effettuato il pagamento tramite bonifico si dovrà usare la `Purchase operation` per comunicare a Conio l'avvenuto pagamento. Questa operazione richiederà una `CryptoProof`, ottenibile nello stesso modo in cui viene ottenuta quella alla signup. A differire sono solo i seguenti dati da concatenare, di cui è importante rispettare l'ordine:

```
[proofID, "PAY_FOR_BID_WT", bidID, userID, Expiration]
```

### Parametri

Un oggetto di tipo `PurchaseParams` contenente:

- **bidId**: l'id della `Bid` da pagare
- **cryptoRequest**: un oggetto di tipo `CryptoRequestBid` configurabile seguendo il riferimento nel paragrafo precedente

### Risposta

Un oggetto di tipo `PurchaseResult` contenente:

- **bidId**: l'id della `Bid` utilizzato per finalizzare il pagamento

### Errori

* INVALID_CRYPTO_PROOF La crypto proof non è valida
* INVALID_PAYMENT_METHOD Il metodo di pagamento non è valido
* UNSUPPORTED_PAYMENT_METHOD Il metodo di pagamento non è supportato
* TRADING_LIMITS_EXCEEDED La bid viola i limiti massimi di acquisto dell'utente
* TRADE_EXPIRED La bid è scaduta
* BID_ALREADY_PAID La bid è già stata pagata
* BID_NOT_YET_PAID La bid non è ancora stata pagata
* UNRECOVERABLE_BID La bid è in errore
* FIAT_AMOUNT_TOO_LOW L'importo in Fiat è inferiore al limite minimo

### Codice

#### Android

```java
BidCryptoRequest bidCryptoRequest = 
  createCryptoRequest() // Your implementation

PurchaseParams params = 
  new PurchaseParams("bidId", bidCryptoRequest);

conio.exchangeService.purchase(params, result -> {
  result.analysis(bid -> {
    // PurchaseResult
  }, error -> {
    // Exception
  });
});
```

#### iOS
```swift
let cryptoRequest = createCryptoRequest() // Your implementation

let params = PurchaseParams(bidID: "bidID", cryptoRequest: cryptoRequest)

conio.exchangeService.purchase(params: params) { result in
 result.analysis(ifSuccess: { bid in
    // PurchaseResult
    }, ifFailure: { error in
    // ServiceError
    })
});
```

## Vendita di Bitcoin
Per poter vendere dei Bitcoin è necessario effettuare due operazioni. La prima è quella di creazione di una `Ask`, ovvero di una richiesta di vendita di una determinata somma di Bitcoin ad un certo prezzo. Si procede poi con il pagamento di tale `Ask`, passando semplicemente l'`askID`. L'SDK firmerà la transazione che sposterà i Bitcoin dal wallet dell'utente, restituendo alla fine l'id della `Ask` completata.

## Creazione della Ask

Per richiedere una `Ask` si dovrà procedere analogamente a quanto visto per la Bid.
Sarà quindi possibile richiedere una `CreatedAsk` in Euro per la vendita di di 50€, o una `CreatedAsk` in Euro per la vendita di 100.000.000 satoshi. 
Una volta inviata la richiesta, si otterrà una `Ask` contenente, tra le altre informazioni un `ID`. Con questo identificativo sarà possibile **aggiornare** la richiesta di Ask per rimandarne la scadenza e per ottenere le informazioni sul tasso di cambio più aggiornate. Questo scenario è utile nei casi in cui tra la richiesta della Ask e l'effettiva azione dell'utente passi del tempo che renderebbe il tasso di cambio obsoleto.
### Parametri

- **(opzionale) id**: l'id della ask, da valorizzare solo in caso di refresh della ask stessa
- **(one of) satoshi**: l'ammontare in satoshi per cui si vuole conoscere il corrispettivo in euro
- **(one of) fiatAmount**: l'ammontare in valuta corrente per cui si vuole conoscere il corrispettivo in satoshi
- **currency**: la valuta dell'operazione

Nell'SDK sono stati implementati dei costruttori che permettono di valorizzare solo un'informazione alla volta, tra **satoshi** o **fiatAmount**. In nessun caso devono essere presenti entrambi contemporaneamente.

### Risposta

Un oggetto di tipo `CreatedAsk` che contiene:

- **id**: l'id utile al refresh o alla finalizzazione della ask
- **currency**: la valuta dell'operazione
- **satoshi**: l'ammontare in satoshi della richiesta d'acquisto
- **fiatAmount**: l'ammontare in valuta corrente della richiesta d'acquisto
- **serviceFees**: le commissioni di servizio per la transazione, espresse in nella **currency** di riferimento
- **minerFees**: le commissioni per scrivere la transazione in blockchain, espresse in satoshi
- **expiration**: il timestamp di scadenza della richiesta di pagamento. Se la bid scade sarà necessario aggiornarla per proseguire
### Errori

* TRADING_LIMITS_EXCEEDED L'utente ha 0 Eur di limiti residui
* NOT_ENOUGH_BTC_AMOUNT solo se non ha btc L'utente non ha alcun bitcoin
* NO_SUCH_SELLER Errore interno del sottosistema di vendita
* NO_SUCH_WALLET Errore interno del sottosistema di wallet

#### iOS
```swift

// Richiesta di vendita per 50€
let params = 
  CreateOrRefreshAskParams(currency: .eur, fiatAmount: 50.0)

// Richiesta di vendita per 100000000 satoshi
let params = 
  CreateOrRefreshAskParams(currency: .eur, satoshi: 100000000)

// Aggiornamento del valore di una Ask esistente  
let params = 
  CreateOrRefreshAskParams(askID: "id", currency: .eur, fiatAmount: 100.0)

conio.exchangeService.createOrRefreshAsk(params: params) { result in
  result.analysis(ifSuccess: { createdAsk in
    // CreatedBid
  }, ifFailure: { error in
    // ServiceError
  })
}
```

#### Android
```java

// Richiesta di vendita per 50€
CreateOrRefreshAskParams params = 
  new CreateOrRefreshAskParams(Currency.EUR, 50d);

// Aggiornamento del valore di una Ask esistente
CreateOrRefreshAskParams params = 
  new CreateOrRefreshAskParams("id", Currency.EUR, 50d);

conio.exchangeService.createOrRefreshAsk(params: params) { result in
 result.analysis(ifSuccess: { createdAsk in
        // CreatedAsk
    }, ifFailure: { error in
        // Exception
    })
});
```

## Utilizzo della Ask 

Ottenuta la `Ask` da utilizzare è possibile procedere con la finalizzazione della vendita. Per effettuare questa operazione basterà passare l'ID della `CreatedAsk` alla `Sell` operation.
### Parametri

Un oggetto di tipo `SellParams` contenente:

- **askId**: l'id della `Ask`

### Risposta

Un oggetto di tipo `SellResult` contenente:

- **askId**: l'id della `Ask` 
### Errori

* TRADING_LIMITS_EXCEEDED La ask viola i limiti massimi di acquisto dell'utente
* TRADE_EXPIRED La ask è scaduta
* UNRECOVERABLE_ASK La ask è in errore
* ASK_ALREADY_PAID La ask è già stata pagata
* NOT_ENOUGH_BTC_AMOUNT_E Bitcoin disponibili non sufficienti
* DUST_ASK Importo in Bitcoin troppo piccolo
* FIAT_AMOUNT_TOO_LOW Importo in Eur troppo basso
### Codice

#### iOS

```swift
let params = SellParams(askID: askID)

conio.exchangeService.sell(params: params) { result in
  result.analysis(ifSuccess: { sellResult in
    // SellResult
  }, ifFailure: { error in
    // ServiceError
  })
}
```

#### Android

```java
SellParams sellParams = new SellParams("askId");

conio.exchangeService.sell(params: params) { result in
 result.analysis(ifSuccess: { sellResult in
        // SellResult
    }, ifFailure: { error in
        // Exception
    })
});
```
