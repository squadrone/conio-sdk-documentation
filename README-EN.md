# Conio #
[![Platform](https://img.shields.io/badge/platform-iOS-lightgrey.svg)]()
[![Platform](https://img.shields.io/badge/platform-Android-lightgrey.svg)]()
[![Pod Version](https://img.shields.io/badge/pod-v0.2.7-blue.svg)]()
[![Artifactory](https://img.shields.io/badge/artifactory-v0.3.1-blue.svg)]()

This SDK makes it simple to integrate a Bitcoin wallet Conio in your app.

## What can you do?

- Create a Bitcoin wallet
- Show wallet details
- Show the historical and current Bitcoin price
- Send and receive Bitcoin
- Buy and Sell Bitcoin
- Show all the transactions made by the wallet

## How does it work?

*   [Installation]()
    *   [Android](https://bitbucket.org/squadrone/conio-sdk-documentation/src/master/pages-en/installation/Android.md)
    *   [iOS](https://bitbucket.org/squadrone/conio-sdk-documentation/src/master/pages-en/installation/iOS.md)
*   [Configuration](https://bitbucket.org/squadrone/conio-sdk-documentation/src/master/pages-en/configuration/Configuration.md)
*   [User](https://bitbucket.org/squadrone/conio-sdk-documentation/src/master/pages-en/user/User.md)
    *   [Terms of service](https://bitbucket.org/squadrone/conio-sdk-documentation/src/master/pages-en/user/User.md)
    *   [Signup](https://bitbucket.org/squadrone/conio-sdk-documentation/src/master/pages-en/user/User.md)
    *   [Login](https://bitbucket.org/squadrone/conio-sdk-documentation/src/master/pages-en/user/User.md)
    *   [Logout](https://bitbucket.org/squadrone/conio-sdk-documentation/src/master/pages-en/user/User.md)
*   [Wallet](https://bitbucket.org/squadrone/conio-sdk-documentation/src/master/pages-en/wallet/Wallet.md)
    *   [Bitcoin address](https://bitbucket.org/squadrone/conio-sdk-documentation/src/master/pages-en/wallet/Wallet.md)
    *   [Movements list](https://bitbucket.org/squadrone/conio-sdk-documentation/src/master/pages-en/wallet/Wallet.md)
    *   [Activity details](https://bitbucket.org/squadrone/conio-sdk-documentation/src/master/pages-en/wallet/Wallet.md)
    *   [Wallet details](https://bitbucket.org/squadrone/conio-sdk-documentation/src/master/pages-en/wallet/Wallet.md)
    *   [Send Bitcoin](https://bitbucket.org/squadrone/conio-sdk-documentation/src/master/pages-en/wallet/Wallet.md)
*   [Market](https://bitbucket.org/squadrone/conio-sdk-documentation/src/master/pages-en/exchange/Exchange.md)
    *   [Bitcoin Price](https://bitbucket.org/squadrone/conio-sdk-documentation/src/master/pages-en/exchange/Exchange.md)
    *   [Historical Price](https://bitbucket.org/squadrone/conio-sdk-documentation/src/master/pages-en/exchange/Exchange.md)
    *   [Trading limits](https://bitbucket.org/squadrone/conio-sdk-documentation/src/master/pages-en/exchange/Exchange.md)
    *   [Trade Bitcoin](https://bitbucket.org/squadrone/conio-sdk-documentation/src/master/pages-en/exchange/Exchange.md)
*   [General exceptions](https://bitbucket.org/squadrone/conio-sdk-documentation/src/master/pages-en/exceptions/Exceptions.md)
    *   [Outdated SDK](https://bitbucket.org/squadrone/conio-sdk-documentation/src/master/pages-en/exceptions/Exceptions.md)
*   [Change Log](#Changelog)

## Changelog

#### 25/08/2021 - **iOS**. `0.2.13`. 
- Update staging certificate

#### 25/08/2021 - **iOS**. `0.2.12`. 
- Bugfix

#### 16/06/2021 - **iOS**. `0.2.10`. 
- Updating certificate pinning

#### 12/05/2021 - **iOS**. `0.2.9`. 
- Updating dependencies

#### 12/04/2021 - **iOS**. `0.2.8`. 
- Bugfix on refresh token operation

#### 04/03/2021 - **iOS**. `0.2.7`. 
- Updated supported ConioErrors

#### 10/02/2021 - **iOS**. `0.2.6`. 
- The SDK now throws the `unauthorized` error only when a 401 error occurs

#### 10/02/2021 - **Android**. `0.3.1`
- `ServiceException`: removed `mfaToken` property
- `ServiceException`: removed `conioError` property
- `ServiceException`: added `serviceError` property
- Added `ConioException` class (subclass of `ServiceException`, with `conioError` property)
- Added `MfaRequiredException` class (subclass of `ServiceException`, with `mfaToken` property)
- Added `UnauthorizedException` class (subclass of `ServiceException`)

#### 09/12/2020 - **iOS**. `0.2.5`. 
- Added ConioError.BID_NOT_YET_PAID

#### 08/10/2020 - **Android**. `0.2.1`
- Exposed, for each service, an overload that return a `DeferredResult` (a specialized alias of the type [kotlinx.coroutines.Deferred](https://kotlin.github.io/kotlinx.coroutines/kotlinx-coroutines-core/kotlinx.coroutines/-deferred/))
- Renamed `NetworkProtocolException` to `OutdatedSDKException`
- Renamed `CreatedBid.wiretransferPayeeInfo` to `CreatedBid.wiretransferInfo`
- Added ConioError.BID_NOT_YET_PAID

#### 08/10/2020 - **Android**. `0.1.138`
- The SDK throw a `NetworkProtocolException` if the SDK version is deprecated.
- Updated purchase flow to support the purchase with wire transfer.

#### 05/10/2020 - **iOS**. `0.2.4`. 
- The SDK throws a `ServiceError.outdatedSDK` if the protocol version is deprecated.
- Updated purchase flow to support the purchase with wire transfer.

#### 07/05/2020 - **Android**. `0.1.125`. 
- Certificate-pinning on the test environment

#### 07/05/2020 - **Android**. `0.1.109`. 
- Error underMaintenance returned when Conio backend is performing maintenance

#### 07/05/2020 - **iOS**. `0.2.3`. 
- Error underMaintenance returned when Conio backend is performing maintenance

#### 01/04/2020 - **iOS**. `0.2.2`. 
- The mnemonic words are generated based on the device language.
