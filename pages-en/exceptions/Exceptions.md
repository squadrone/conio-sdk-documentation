# General Exceptions

## ConioError

This error wrap all the possible error responses directly bonded to the operations.

For examples, let's take the `conio.walletService.withdrawalFees` operation: if an user has 1 bitcoin
and asks for the mining fees to send 50 bitcoin, he will get an `NO_SUCH_WITHDRAWAL_FEES_INFO`.

List of the possible conio errors:

```
APP_IMPROVEMENT_ACCEPTANCE_NOT_ACCEPTED
ASK_ALREADY_PAID
BID_ALREADY_PAID
BID_EXPIRED
BID_IS_IN_ERROR
BID_NOT_YET_PAID
BITHUSTLER_SERVICE_COULD_NOT_CREATE_SELLER
CARDS_LIMITS_EXCEEDED
CARDS_SERVICE_COULD_NOT_CREATE_PAYER
CLIENT_SUPPORT_ACCEPTANCE_NOT_ACCEPTED
DUPLICATE_EMAIL_ADDRESS
DUST_ASK
DUST_TRANSACTION
FIAT_AMOUNT_TOO_LOW
INCONSISTENT_STATE
INCONSISTENT_TRANSACTION
INVALID_IBAN
INVALID_MESSAGE_SIGNATURE
INVALID_PAYMENT_METHOD
INVALID_TOKEN
INVALID_TOKEN_PAYLOAD
MULTIPLE_SELL_METHODS
NO_SUCH3D_SECURE
NO_SUCH_SELL_METHOD
NO_SUCH_SELLER
NO_SUCH_WALLET
NO_SUCH_WITHDRAWAL_FEES_INFO
NOT_ENOUGH_BTC_AMOUNT
TRADE_EXPIRED
TRADING_LIMITS_EXCEEDED
UNAVAILABLE_BTC_SUBSYSTEM
UNRECOVERABLE_ASK
UNRECOVERABLE_BID
UNSUPPORTED_PAYMENT_METHOD
WALLET_ALREADY_CREATED_WITH_DIFFERENT_KEYS
WALLET_ALREADY_OWNED_BY_ANOTHER_USER
```

### Code

#### Android

```java
WithdrawalFeesParams params = new WithdrawalFeesParams(
    "mkHS9ne12qx9pS9VojpwU5xtRd4T7X7ZUt",
    100000000,
    TransactionSpeedType.SPEED_FIVE
);

conio.walletService.withdrawalFees(params, result -> result.analysis(
        fees -> { /* On success */ },
        error -> {
            ConioException conioException = (ConioException) error;
            if (conioException.getConioError() == ConioError.NO_SUCH_WITHDRAWAL_FEES_INFO) {
                /* Handle error */
            }
        }
));
```

#### iOS
```swift
// EXAMPLE: the user asks for the fees for a very big amount
  let params = WithdrawalFeesParams(destAddress: "mkHS9ne12qx9pS9VojpwU5xtRd4T7X7ZUt", amount: 100000000, speed: .transactionSpeedType5)
        
conio.walletService.withdrawalFees(params: params) { result in
    result.analysis(ifSuccess: { availableFees in
        // success
    }, ifFailure: { error in
        if case .conioerror(error: ConioError.NO_SUCH_WITHDRAWAL_FEES_INFO) = error {
            // can't calculate the mining fees 
        }
        // ServiceError
    })
}
```

## Unauthorized

This error is thrown when you are not authorized to use a method for one of the following reasons:

- using a method that requires authentication without a valid session;
- trying to login with wrong credentials.

Make sure you have a valid session by re-authenticating with a [login](https://bitbucket.org/squadrone/conio-sdk-documentation/src/master/pages/user/User.md#Login) or [sign-up](https://bitbucket.org/squadrone/conio-sdk-documentation/src/master/pages/user/User.md#Signup).

### Codice

#### Android

```java
UserLogin user = new UserLogin("username", "wrong_password");

conio.userService.login(user, result -> result.analysis(
    acceptances -> {},
    error -> {
        if (error instanceof UnauthorizedException) {
            /* Handle the error */
        }

        // Or

        ServiceException serviceException = (ServiceException) error;
        if (serviceException.getServiceError() == ServiceError.UNAUTHORIZED) {
            /* Handle the error */
        }
    }
));

```
#### iOS

```swift
conio.userService.login(with: login) { result in
    result.analysis(ifSuccess: { acceptances in
        /* Handle success */
    }, ifFailure: { error in
        // ServiceError
        if case .unauthorized = error {
            /* Handle the error */
        }
    })
}
```

## Outdated SDK

This error is thrown when the user is trying to use a deprecated version of the SDK.

We recommend to handle this error to notify the user to update the app.

### Code

#### Android

```java
LegalAcceptancesParams params = new LegalAcceptancesParams(Language.ITALIAN);

conio.userService.getLegalAcceptances(params, result -> {
    result.analysis(acceptances -> {},
    error -> {
        if (error instanceof OutdatedSDKException) {
            /* Handle the error */
        }

        // Or

        ServiceException serviceException = (ServiceException) error;
        if (serviceException.getServiceError() == ServiceError.OUTDATED_SDK) {
            /* Handle the error */
        }
    });
});
```

#### iOS

```swift
let params = LegalAcceptancesParams(language: .italian)
conio.userService.getLegalAcceptances(params: params) { result in
    result.analysis(ifSuccess: { legalAcceptances in
    }, ifFailure: { error in
        // ServiceError
        if case .outdatedSDK = error {
            print("Please update the SDK")
        }
    })
}
```
