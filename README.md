# Conio #
[![Platform](https://img.shields.io/badge/platform-iOS-lightgrey.svg)]()
[![Platform](https://img.shields.io/badge/platform-Android-lightgrey.svg)]()
[![Pod Version](https://img.shields.io/badge/pod-v0.2.4-blue.svg)]()
[![Artifactory](https://img.shields.io/badge/artifactory-v0.1.132-blue.svg)]()

Questo SDK rende semplice integrare un portafoglio Bitcoin Conio nella propria app.

## Cosa si può fare?

- Creare un portafoglio Bitcoin
- Ottenere le informazioni del portafoglio creato
- Inviare e ricevere fondi
- Comprare e Vendere Bitcoin
- Leggere il dettaglio storico delle operazioni di un utente
- Ottenere il prezzo storico ed attuale del Bitcoin

## Come posso utilizzarlo?

*   [Installazione]()
    *   [Android](https://bitbucket.org/squadrone/conio-sdk-documentation/src/master/pages/installation/Android.md)
    *   [iOS](https://bitbucket.org/squadrone/conio-sdk-documentation/src/master/pages/installation/iOS.md)
*   [Configurazione](https://bitbucket.org/squadrone/conio-sdk-documentation/src/master/pages/configuration/Configuration.md)
*   [Operazioni sull'utente](https://bitbucket.org/squadrone/conio-sdk-documentation/src/master/pages/user/User.md)
    *   [Lettura termini di servizio](https://bitbucket.org/squadrone/conio-sdk-documentation/src/master/pages/user/User.md)
    *   [Signup](https://bitbucket.org/squadrone/conio-sdk-documentation/src/master/pages/user/User.md)
    *   [Login](https://bitbucket.org/squadrone/conio-sdk-documentation/src/master/pages/user/User.md)
    *   [Logout](https://bitbucket.org/squadrone/conio-sdk-documentation/src/master/pages/user/User.md)
*   [Operazioni sul portafoglio](https://bitbucket.org/squadrone/conio-sdk-documentation/src/master/pages/wallet/Wallet.md)
    *   [Lettura dell'indirizzo attuale](https://bitbucket.org/squadrone/conio-sdk-documentation/src/master/pages/wallet/Wallet.md)
    *   [Lista dei movimenti](https://bitbucket.org/squadrone/conio-sdk-documentation/src/master/pages/wallet/Wallet.md)
    *   [Dettaglio di un movimento](https://bitbucket.org/squadrone/conio-sdk-documentation/src/master/pages/wallet/Wallet.md)
    *   [Dettagli del portafoglio](https://bitbucket.org/squadrone/conio-sdk-documentation/src/master/pages/wallet/Wallet.md)
    *   [Invio di Bitcoin](https://bitbucket.org/squadrone/conio-sdk-documentation/src/master/pages/wallet/Wallet.md)
*   [Operazioni sul mercato](https://bitbucket.org/squadrone/conio-sdk-documentation/src/master/pages/exchange/Exchange.md)
    *   [Prezzo corrente del Bitcoin](https://bitbucket.org/squadrone/conio-sdk-documentation/src/master/pages/exchange/Exchange.md)
    *   [Prezzo storico del Bitcoin](https://bitbucket.org/squadrone/conio-sdk-documentation/src/master/pages/exchange/Exchange.md)
    *   [Recupero dei limiti di trading](https://bitbucket.org/squadrone/conio-sdk-documentation/src/master/pages/exchange/Exchange.md)
    *   [Acquisto di Bitcoin](https://bitbucket.org/squadrone/conio-sdk-documentation/src/master/pages/exchange/Exchange.md)
*   [Eccezioni possibili](https://bitbucket.org/squadrone/conio-sdk-documentation/src/master/pages/exceptions/Exceptions.md)
    *   [SDK obsoleto](https://bitbucket.org/squadrone/conio-sdk-documentation/src/master/pages/exceptions/Exceptions.md)
*   [Change Log](#cos'è-cambiato-dall'ultima-versione)

## Cos'è cambiato dall'ultima versione?

#### 11/02/2021 - **iOS**. `0.2.13`. 
- Aggiornamento certificato staging

#### 25/08/2021 - **iOS**. `0.2.12`. 
- Bugfix

#### 16/06/2021 - **iOS**. `0.2.10`. 
- Aggiornamento certificate pinning

#### 12/05/2021 - **iOS**. `0.2.9`. 
- Aggiornamento dipendenze

#### 12/04/2021 - **iOS**. `0.2.8`. 
- Bugfix sulla refresh token operation

#### 04/03/2021 - **iOS**. `0.2.7`. 
- Aggiornamento dei ConioErrors supportati

#### 10/02/2021 - **iOS**. `0.2.6`. 
- L'SDK ora restituisce un errore `unauthorized` solo a fronte di un codice 401

#### 10/02/2021 - **Android**. `0.3.1`
- `ServiceException`: rimossa proprietà `mfaToken`
- `ServiceException`: rimossa proprietà `conioError`
- `ServiceException`: aggiunta proprietà `serviceError`
- Aggiunta classe `MfaRequiredException` (sotto classe di `ServiceException`, con proprietà `mfaToken`)
- Aggiunta classe `UnauthorizedException` (sotto classe di `ServiceException`)
- Aggiunta classe `ConioException` (sotto classe di `ServiceException`, con proprietà `conioError`)

#### 09/12/2020 - **iOS**. `0.2.5`. 
- Aggiunto ConioError.BID_NOT_YET_PAID

#### 08/10/2020 - **Android**. `0.2.1`
- Esposto, per ogni servizio, un overload che restituisce un oggetto di tipo `DeferredResult` (un alias specializzato del tipo [kotlinx.coroutines.Deferred](https://kotlin.github.io/kotlinx.coroutines/kotlinx-coroutines-core/kotlinx.coroutines/-deferred/))
- Rinominato `NetworkProtocolException` in `OutdatedSDKException`
- Rinominato `CreatedBid.wiretransferPayeeInfo` in `CreatedBid.wiretransferInfo`
- Aggiunto ConioError.BID_NOT_YET_PAID

#### 08/10/2020 - **Android**. `0.1.138`
- L'SDK ora ritorna errore `NetworkProtocolException` se la versione dell'SDK è deprecata.
- Aggiornato il flusso di acquisto per supportare acquisto con bonifico.

#### 05/10/2020 - **iOS**. `0.2.4`. 
- L'SDK ora ritorna un `ServiceError.outdatedSDK` se la versione dell'SDK e' deprecata.
- Aggiornato il flusso di acquisto per supportare acquisto con bonifico.

#### 07/05/2020 - **Android**. `0.1.125`. 
- Supporto per certificate-pinning sull'ambiente di test.

#### 07/05/2020 - **Android**. `0.1.109`. 
- L'SDK ora ritorna errore underMaintenance quando Conio e' in manutenzione.

#### 07/05/2020 - **iOS**. `0.2.3`. 
- L'SDK ora ritorna errore underMaintenance quando Conio e' in manutenzione.

#### 01/04/2020 - **iOS**. `0.2.2`. 
- L'sdk ora genera la mnemonica in base alla lingua del dispositivo.
